<?php
$ar_names=array("test.jpg","foot.jpg","hand.jpg","arkiv.jpg","rug.jpg","rug2.jpg","rug3.jpg","painting.pnm");
$html="";
$html_file="images_planes.html";
$newsize=60;

foreach($ar_names as $file_orig){
  $root_name=substr($file_orig,0,-4);
  $name=$root_name."_obj_".$newsize."_";
  $html.=generate_planes($file_orig,$name,$newsize);
}
write_html($html_file,$html);
system("firefox ".$html_file);


function generate_planes($file_orig,$name,$newsize){

  $scale_down=array(100,80,60,40,20,10);
  $sampled_down=array();
  $sampled_up=array();
  $html="<div style=\"width:1600px;\">";
  foreach(range(1,10,1) as $s){
    $sh="convert -resize ".$s."x".$s." ".$file_orig." ".$name.$s.".jpg";
    print $sh."\n";
    system($sh);
    $sampled_down[]=$name.$s.".jpg";
  }
  foreach(range(10,50,5) as $s){
    $sh="convert -resize ".$s."x".$s." ".$file_orig." ".$name.$s.".jpg";
    print $sh."\n";
    system($sh);
    $sampled_down[]=$name.$s.".jpg";
  }
  foreach(range(50,100,10) as $s){
    $sh="convert -resize ".$s."x".$s." ".$file_orig." ".$name.$s.".jpg";
    print $sh."\n";
    system($sh);
    $sampled_down[]=$name.$s.".jpg";
  }
  foreach($sampled_down as $d){
    $html.="<img src=\"".$newsize."_".$d.".jpg\" />";
    $sh="convert -sample ".$newsize."x".$newsize." ".$d." ".$newsize."_".$d.".jpg";
    print $sh."\n";
    system($sh);
    $sampled_up[]=$newsize."_".$d.".jpg";
  }
  $html."</div>";
  return $html;
}

function write_html($html_file,$html){
  if (!$handle = fopen($html_file, 'w+')) {
    echo "Cannot open file ($html_file)";
    exit;
  }

  // Write $somecontent to our opened file.
  if (fwrite($handle, $html) === FALSE) {
    echo "Cannot write to file ($html_file)";
    exit;
  }

  echo "Success";

  fclose($handle);
}

?>