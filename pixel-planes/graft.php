<?php
$im_base="sky.jpg";
$ar_planes=array();
$directory = '.';
$files=scandir($directory);
foreach($files as $f){
  $expr="/imagePixelated-".$im_base."/";
  if(preg_match($expr,$f)){
    $ar_planes[]=$f;
  }
}
for($x=0;$x<100;$x++){

  print "image $x \n";
  $dest = imagecreatefromjpeg($im_base);
  $pass=rand(10,100);
  for($i=0;$i<$pass;$i++){
    $pos=rand(0,count($ar_planes)-1);
    $src = imagecreatefrompng($ar_planes[$pos]);

    // Copy
    $width = imagesx($src);
    $height = imagesy($src);
    $wpos=rand(0,$width);
    $hpos=rand(0,$height);
    $wavail=round($width-$wpos);
    $havail=round($height-$hpos);
    $w=rand(10,$wavail);
    $h=rand(10,$havail);

    imagecopy($dest, $src, $wpos, $hpos, $wpos, $hpos,$w,$h);

  }
  // Output the image
  imagejpeg($dest,"output-".$im_base."-".$x.".jpg");

  // Free up memory
  imagedestroy($src);
  imagedestroy($dest);
}
?>