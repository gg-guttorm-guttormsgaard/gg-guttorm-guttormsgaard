<?php  
$iname="sky.jpg";
$img = imagecreatefromjpeg($iname);
$width = imagesx($img);
$height = imagesy($img);
foreach(range(5,50,5) as $s){
  print "Plane $s \n";
  // Create 5% version of the original image:
  $newImg = imagecreatetruecolor($width,$height);
  imagecopyresized($newImg,$img,0,0,0,0,round($width / $s),round($height / $s),$width,$height);
  // Create 100% version ... blow it back up to it's initial size:
  $newImg2 = imagecreatetruecolor($width,$height);
  imagecopyresized($newImg2,$newImg,0,0,0,0,$width,$height,round($width / $s),round($height / $s));
  // No need for a jpeg here :-)
  /* imagepng($newImg,"imagePixelated-0.png"); */
  imagepng($newImg2,"imagePixelated-".$iname."-".$s.".png");
}
?>