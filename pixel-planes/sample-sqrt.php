<?php
$html_file="test.html";
$file_orig="foot.jpg";
$name="sqrt_footobj_";
$scale_down=array(100,80,60,40,20,10);
$sampled_down=array();
$newsize=200;
$sampled_up=array();
$html="";
/* foreach(range(5,100,5) as $s){ */
/*   $sh="convert -resize ".$s."x".$s." ".$file_orig." ".$name.$s.".jpg"; */
/*   print $sh."\n"; */
/*   system($sh); */
/*   $sampled_down[]=$name.$s.".jpg"; */
/* } */

$w=200;
$h=200;
$steps=20;

for($pa=$w*$h;$pa>=1;$pa-=$w*$h/$steps){
  $pixsize=sqrt($pa);
  /* print round($w/$pixsize)." ".round($pixsize)." ".$pa."\n"; */
  print round($w/$pixsize)." ".round($pixsize)." ".$pa."\n";
  $s=round($w-$pixsize);
  $sh="convert -resize ".$s."x".$s." ".$file_orig." ".$name.$s.".jpg";
  print $sh."\n";
  system($sh);
  $sampled_down[]=$name.$s.".jpg";

}

foreach($sampled_down as $d){
  $html.="<img src=\"".$newsize."_".$d.".jpg\" />";
  $sh="convert -sample ".$newsize."x".$newsize." ".$d." ".$newsize."_".$d.".jpg";
  print $sh."\n";
  system($sh);
  $sampled_up[]=$newsize."_".$d.".jpg";
}
print $html;
// Let's make sure the file exists and is writable first.
if (is_writable($html_file)) {

    // In our example we're opening $filename in append mode.
    // The file pointer is at the bottom of the file hence
    // that's where $somecontent will go when we fwrite() it.
  if (!$handle = fopen($html_file, 'w')) {
         echo "Cannot open file ($html_file)";
         exit;
    }

    // Write $somecontent to our opened file.
    if (fwrite($handle, $html) === FALSE) {
        echo "Cannot write to file ($html_file)";
        exit;
    }

    echo "Success";

    fclose($handle);

} else {
    echo "The file $html_file is not writable";
}
system("firefox ".$html_file);
?>